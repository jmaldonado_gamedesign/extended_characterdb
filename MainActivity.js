var Character = require("./character").Character;
var fs = require('fs');
const db = require('./db');
var tempCharacter;
var listCharacters = [];
var JSONtext = 'list.json';
var fileFolder = './files/'


printWelcomeMessage();
interactiveConsole();

function input(question, callback)
{
  var stdin = process.stdin, stdout = process.stdout;
  stdin.resume();
  stdout.write(question);
  stdin.once('data', function(data)
  {
    data = data.toString().trim();
    callback(data);
  });
}

function interactiveConsole()
{
  input( ">> ", function(data) {

      var parts=data.trim().split(' ');
      if (parts[0]) parts[0]=parts[0].toUpperCase();

      switch (true) {

          //EXIT______________________________________________
          case (parts[0]=="Q"):
            process.exit(0);
            break;


          //LIST______________________________________________
          case (parts[0]=="LIST"):
            ShowList();
            break;


          //ADD______________________________________________
          case (parts[0]=="ADD"):
            if(parts.length < 4) //then the user has not added the specific things for the adding
              console.log("NOT ADDED the necessary camps to create the character");
            else{
              AddCharacter(parts[1], parts[2], parts[3], true);
            }
            break;


          //DELETE______________________________________________
          case (parts[0]=="DEL"):
            if(parts.length < 2){
              console.log("The NAME is missing. Cannot delete")
            }
            else{
              DeleteName(parts[1]);
            }
            break;


          //SAVE______________________________________________
          case (parts[0]=="SAVE"):
            if(parts.length < 2){
              console.log("FileName not added, it will create it with the name 'list.json' ");
              JSONtext = fileFolder + 'list.json';
            }
            else {
              JSONtext = fileFolder + parts[1];
            }

            SaveToJSON(JSONtext);
            break;


          //LOAD_______________________________________________
          case (parts[0]=="LOAD"):
            if(parts.length < 2){
              console.log("FileName is missing. Please input one");
              break;
            }

            JSONtext = fileFolder + parts[1];
            LoadFromJSON(JSONtext);
            break;

          case(parts[0] == "DBLOAD"):
            LoadfromDB();
            break;

          case(parts[0] == "DBSAVE"):
            SaveToDB();
            break;

          //HELP_______________________________________________
          case (parts[0]=="HELP"):
            console.clear();
            printWelcomeMessage();
            break;


          default:
            console.log("Incorrect command");
            break;
        }
        interactiveConsole();
    });
}


function ShowList(){
  console.log("CHARACTERS:")
  for(let i = 0; i < listCharacters.length; i++)
    console.log(listCharacters[i].name + " -> " + listCharacters[i].power.toString() + " - " + listCharacters[i].type);
}

function AddCharacter(name, power, type, showmessages = false){

  if(listCharacters.length != 0){
      for(let i = 0; i < listCharacters.length; i++){
        if(name == listCharacters[i].name){
          console.log("Character already exists in the list. Cannot Add")
          break;
        }
        else if(name != listCharacters[i].name && (i + 1) == listCharacters.length){
          tempCharacter = new Character(name, power, type);
          listCharacters.push(tempCharacter);
          if(showmessages) console.log("Added the character!");
          break;
        }
      }
    }
    else{
      tempCharacter = new Character(name, power, type);
      listCharacters.push(tempCharacter);
      if(showmessages) console.log("Added the character!");
    }
}

function DeleteName(name){


  if(listCharacters.length != 0){
    for(let i = 0; i < listCharacters.length; i++){
      if(listCharacters[i].name == name){
        listCharacters.splice(i, 1) //splice removes from an array a specified index.
        console.log("Character Deleted!")
        break;
      }
      else if(i + 1 == listCharacters.length ){
        console.log("Character not found. Cannot Delete")
      }

    }
  }else console.log("Nothing added in the list. Cannot Delete")

}

function SaveToJSON(fileName){

  var myJSON = JSON.stringify(listCharacters);
  fs.writeFile(fileName, myJSON, 'utf8', function readFileCallback(err, data){

    if(err){
      console.log(err)
    }});
}

function LoadFromJSON(fileName){

  fs.readFile(fileName, 'utf8', function readFileCallback(err, data){

    if(err){
      console.log(err)
    }else{
      listCharacters = JSON.parse(data);
    }
  });
}

function SaveToDB(){

  if(listCharacters.length > 0){

    db.query("DELETE from characterlist", [], (err,data)=>{

      if(err){
        console.log("Error: ", err);
        return;
      }
    });

    for(let i = 0; i < listCharacters.length; i++){
      db.query("INSERT INTO characterlist(charactername, powernum, powertype) VALUES($1,$2, $3)", [listCharacters[i].name, listCharacters[i].power, listCharacters[i].type], (err, data)=>{

        if(err){
          console.log("Error: " + err);
          return;
        }
      });
      console.log("SAVE TO DB DONE!");
    }
  }
  else{
    console.log("THERE IS NO CHARACTERS ON THE LIST! please input something!");
  }
}

function LoadfromDB(){

  db.query("SELECT * from characterlist", [], (err,data)=>{

    if(!err){
      listCharacters=[];
      for(let i = 0; i < data.rowCount; i++){
        AddCharacter(data.rows[i].charactername, data.rows[i].powernum, data.rows[i].powertype);
      }
      console.log("LOAD FROM DB DONE!");
      return;
    }
    else
      console.log("Error: " + err);
      return;
  });

}

function printWelcomeMessage() {
          console.log(`Avaialble commands:
Q - Exits the program
LIST                        - Shows all the characters
ADD [name] [power] [type]   - Enters a character
DEL [name]                  - Deletes a character if it exist in the list
SAVE [filename]             - Save in a JSON file the list
LOAD [filename]             - Loads a JSON file with a list
DBSAVE                      - Saves the current character list to the Datbase
DBLOAD                      - Loads a new character list from the Database
HELP                        - Reshows this message`);
}
