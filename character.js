class Character{

  constructor(_name, _power, _type){
    this.name = _name;
    this.power = _power;
    this.type = _type;
  }
}

exports.Character = Character;
